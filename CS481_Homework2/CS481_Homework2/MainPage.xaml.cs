﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_Homework2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
		//look at this if this grid in MainPage.xaml doesn't work
		//https://www.c-sharpcorner.com/article/how-to-create-a-simple-calculator-using-grid-layout-in-xamarin-forms2/
		//or xamarin forms docs


		//Heavily referenced from
		//https://github.com/xamarin/mobile-samples/blob/master/LivePlayer/BasicCalculator/Calculator/MainPage.xaml.cs
		//cState(current state) operation (add, subtract, etc.) and num1/num2 are created/initialized here
		int cState = 1; string operation; double num1, num2;
        public MainPage()
        {InitializeComponent();}
        void OnSelectNumber(object sender, EventArgs e)
        {
			//runs following code snippet once button is pressed
			Button button = (Button)sender;
			string pushed = button.Text;

			if (this.resultText.Text == "0" || cState < 0)
			{   
				//the value gets deleted when a button press is detected
				this.resultText.Text = ""; 
				if (cState < 0) {cState *= -1;}
			}
			this.resultText.Text += pushed;

			double number;
			if (double.TryParse(this.resultText.Text, out number))
			{
				//ToString("N0") is supposed to print the value with comma separators and no decimal points, or something to that effect
				this.resultText.Text = number.ToString("N0");
				if (cState == 1){ num1 = number;}
				else{ num2 = number;}
			}
		}

		void OnSelectOperator(object sender, EventArgs e)
		{
			cState = -2;
			Button button = (Button)sender;
			string pushed = button.Text;
			operation = pushed;
		}

		void OnClear(object sender, EventArgs e)
		{	//resets the calculator
			num1 = 0; num2 = 0; cState = 1;
			this.resultText.Text = "0";
		}

		void OnCalculate(object sender, EventArgs e)
		{
			if (cState == 2)
			{
				var result = ActualCalculator(num1, num2, operation);
				// "this" is used instead of mainpage.resultText to save on time
				this.resultText.Text = result.ToString();
				num1 = result;
				cState = -1;
			}

			//Takes the operator and uses the switch function below to perform the corresponding arithmetic
		double ActualCalculator(double num1, double num2, string mathOperator)
			{
				//initialized result as 0 so it doesn't affect the calculations
				double num3 = 0;

				//Basic arithmetic for the calculations function, add/subtract/mult/divide
				switch (mathOperator)
				{
					case "+":
						num3 = num1 + num2;
						break;
					case "-":
						num3 = num1 - num2;
						break;
					case "×":
						num3 = num1 * num2;
						break;
					case "÷":
						num3 = num1 / num2;
						break;
				}

				return num3;
			}
		}
	}
}